<?php
require_once "db.php";

class autentification
{
    public $status;
    public $message;
    public $parametres = [];

    public function __construct($api_key, $email_address, $password, $device_token)
    {
        $this->parametres['api_key'] = $api_key;
        $this->parametres['email_address'] = $email_address;
        $this->parametres['password'] =md5($password);
        $this->parametres['device_token'] = $device_token;
        $this->isValid();

    }

    private function isValid()
    {
        $dbc = db::getInstance();
        $dbc->getQuery('SET NAMES UTF-8');
        $result = $dbc->getQuery("SELECT `email_address`, `password` FROM `users` WHERE `email_address`='{$this->parametres['email_address']}' AND `password`='{$this->parametres['password']}'");
        $rows = $result->fetch_assoc();
        if ($rows['email_address']!= '')
        {
            $this->status = "succes";
            $this->message = "Logged is succesfully.";
            $date = date("Y-m-d G-i-s");
            $dbc->getQuery("UPDATE `users` SET `date_last_login` ='{$date}' WHERE `email_address`='{$this->parametres['email_address']}'");
            session_start();

        }
        else
        {
            $this->status = "error";
            $this->message = "Incorrect email or password.";
            session_destroy();

        }
    }

    public function createResponse()
    {
        $response = [];
        $dbc = db::getInstance();
        $dbc->getQuery('SET NAMES UTF-8');
        $result = $dbc->getQuery("SELECT `email_address`, `id`, `name_first`, `name_last` FROM `users` WHERE `email_address`='{$this->parametres['email_address']}'");
        $rows = $result->fetch_assoc();

        $response['status'] = $this->status;
        $response['message'] = $this->message;
        $response['data']['name_first'] = $rows['name_first'];
        $response['data']['name_last'] = $rows['name_last'];
        $response['data']['email_address'] = $rows['email_address'];
        $response['data']['device_token'] = $this->parametres['device_token'];
        $response['data']['session_status'] = session_status();
        $response['data']['session_id'] = session_id();

        return json_encode($response);


    }

}