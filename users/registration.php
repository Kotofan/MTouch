<?php
require_once "db.php";

class Registration
{
    public $status;
    public $message;
    public $parametres = [];

    public function __construct($api_key, $name_first, $name_last, $email_address, $password, $device_token)
    {
        $this->parametres['api_key'] = $api_key;
        $this->parametres['name_first'] = $name_first;
        $this->parametres['name_last'] = $name_last;
        $this->parametres['email_address'] = $email_address;
        $this->parametres['password'] =md5($password);
        $this->parametres['device_token'] = $device_token;
        $this->message = "User registered successfully.";
        $this->status = "success";

        if($this->ifIssetAccount()== true)
            session_start();

        
    }

    private function ifIssetAccount()
    {
        $dbc = db::getInstance();
        $dbc->getQuery('SET NAMES UTF-8');
        $result = $dbc->getQuery("SELECT * FROM users");
        $rows = $result->fetch_assoc();
        do{
            if($rows['email_address'] == $this->parametres['email_address']){
                $this->message = "User already exists.";
                $this->status = "error";
                return false;
            }
        } while($rows=$result->fetch_assoc());

        $this->inputData();

        return true;


    }

    private function inputData()
    {
        $dbc = db::getInstance();
        $dbc->getQuery('SET NAMES UTF-8');
        $date = date("Y-m-d G-i-s");

        $dbc->getQuery("INSERT INTO `users`(`id`, `api_key`, `name_first`, `name_last`, `email_address`, `password`, `device_token`,`date_created`,`date_last_login`) VALUES (NULL ,'{$this->parametres['api_key']}','{$this->parametres['name_first']}','{$this->parametres['name_last']}','{$this->parametres['email_address']}','{$this->parametres['password']}','{$this->parametres['device_token']}','{$date}','{$date}')");


    }
    public function createResponse()
    {
        $dbc = db::getInstance();
        $dbc->getQuery('SET NAMES UTF-8');
        $result = $dbc->getQuery("SELECT * FROM `users` WHERE `email_address`='{$this->parametres['email_address']}'");
        $rows = $result->fetch_assoc();
        $response = [];
        $response['status'] = $this->status;
        $response['message'] = $this->message;
        $response['data']['id'] = $rows['id'];
        $response['data']['name_first'] = $rows['name_first'];
        $response['data']['name_last'] = $rows['name_last'];
        $response['data']['email_address'] = $rows['email_address'];
        $response['data']['device_token'] = $rows['device_token'];
        $response['data']['session_status'] = session_status();
        $response['data']['session_id'] = session_id();
        return json_encode($response);
    }
    
}